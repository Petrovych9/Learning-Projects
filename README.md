# Welcome!

---

## Branches:
- **main** - main app - Foodclub
- **foodclub** - history of developing Foodclub until 6.07.2023
- **bms** - Book Management System - another project, simpler than Foodclub
- **projects** - a branch that should contain both projects
- **master** - a few initial commits

## Sections:
1. Description
2. Functionalities

## Description:
The project is a cooking recipe management system called Foodclub. It provides users with a platform to discover and share cooking recipes. The system focuses on creating a user-friendly experience, allowing users to create, store, and view recipes. It also includes an authentication system to ensure secure access to user-specific features.

## Functionalities:
1. **Authentication System:** 
   - The project includes a robust authentication system that allows users to create accounts, securely log in, and manage their personal information. Only authorized users can access features such as recipe creation and storage.

2. **Recipe Management:**
   - Users can create and store their recipes within the system. They can add ingredients, write cooking instructions, and provide additional details. The system offers intuitive tools to help users easily manage and organize their collection of recipes.

3. **Search and Discovery(soon):**
   - The project provides search and discovery capabilities, allowing users to find recipes based on keywords, ingredients, or specific categories. Personalized recommendations enhance the user experience and help users explore new recipes.

4. **Categories:**
   - The system incorporates a categorization feature, enabling users to classify recipes based on different categories, such as cuisine type or dietary restrictions. Additionally, users can assign tags to further categorize recipes based on specific themes or attributes.

5. **Admin Panel:**
   - The project includes an admin panel with advanced functionalities for system administrators. Administrators can manage user roles, control access privileges, modify user information, view all recipes and users, and perform administrative tasks to ensure smooth operation of the system.

These functionalities work together to create a comprehensive cooking recipe management system that caters to the needs of both users and administrators.

