import pytest
from flask import url_for

from app import menu


def test_menu(client, app):
    expected_menu = {
        'Home': url_for('main.home'),
        'Categories': url_for('main.categories'),
        'All recipes': url_for('main.all_recipes'),
        'New recipe': url_for('main.new_recipe'),
        'My drafts': url_for('main.draft_recipes'),
        'Profile': url_for('main.profile'),
        'Sign In': url_for('auth.signin'),
        'Log out': url_for('auth.logout'),
        'Sign Up': url_for('auth.signup')
    }

    with app.app_context():
        assert menu.menu() == expected_menu
